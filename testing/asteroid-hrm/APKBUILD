# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=asteroid-hrm
pkgver=0_git20230128
pkgrel=0
_commit="e0916fe1af56d81c4ce50471f7a67404f89663f4"
pkgdesc="Default heart-rate-monitor app for AsteroidOS"
url="https://github.com/AsteroidOS/asteroid-hrm"
# armhf blocked by qt5-qtsensors
arch="all !armhf"
license="GPL-3.0-or-later"
depends="
	mapplauncherd
	qt5-qtsensors
	"
makedepends="
	extra-cmake-modules
	qml-asteroid-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	samurai
	"
source="https://github.com/AsteroidOS/asteroid-hrm/archive/$_commit/asteroid-hrm-$_commit.tar.gz"
options="!check" # No tests
builddir="$srcdir/$pkgname-$_commit"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
c3ae9ff18ce50e2aac75f91a90f21ae3a9e6d7cd6f82ca80da54e32f5c18cd9359765f6822f7acecad13b9e8f246a55dcd36a1cd3cf9fd50378cb87906c503ad  asteroid-hrm-e0916fe1af56d81c4ce50471f7a67404f89663f4.tar.gz
"
