# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=asteroid-calculator
pkgver=0_git20230318
pkgrel=0
_commit="3d746f049137316d9c2cd8e9b79f721966014574"
pkgdesc="Default calculator app for AsteroidOS"
url="https://github.com/AsteroidOS/asteroid-calculator"
# armhf blocked by qml-asteroid
arch="all !armhf"
license="GPL-3.0-or-later"
depends="mapplauncherd"
makedepends="
	extra-cmake-modules
	qml-asteroid-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	"
source="https://github.com/AsteroidOS/asteroid-calculator/archive/$_commit/asteroid-calculator-$_commit.tar.gz"
builddir="$srcdir/$pkgname-$_commit"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
25fac417074d5b5109746a7cc1e510dd99ccad58a2d5945f8870a7ec085f3506fab5c209b15ffaac1c21ec6ac12023e4fa6121edf5f9beac3de7130dc199763b  asteroid-calculator-3d746f049137316d9c2cd8e9b79f721966014574.tar.gz
"
