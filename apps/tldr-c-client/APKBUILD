# Contributor: Sergiy Stupar <owner@sestolab.pp.ua>
# Maintainer: Sergiy Stupar <owner@sestolab.pp.ua>
pkgname=tldr-c-client
pkgver=1.6.1
pkgrel=0
pkgdesc="C command-line client for tldr pages"
url="https://github.com/tldr-pages/tldr-c-client"
arch="all"
license="MIT"
depends="!tldr-python-client"
makedepends="curl-dev libzip-dev musl-fts-dev"
subpackages="$pkgname-bash-completion $pkgname-fish-completion $pkgname-zsh-completion $pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/tldr-pages/tldr-c-client/archive/v$pkgver.tar.gz"
options="!check" # No tests

prepare() {
	default_prepare
	sed -i '/^ALL_LDLIBS/s/.*/& -lfts/' Makefile
}

build() {
	make
}

package() {
	install -Dm755 tldr -t "$pkgdir"/usr/bin/
	install -Dm644 man/tldr.1 -t "$pkgdir"/usr/share/man/man1/
	install -Dm644 autocomplete/complete.bash -t "$pkgdir"/usr/share/bash-completion/completions/
	install -Dm644 autocomplete/complete.fish -t "$pkgdir"/usr/share/fish/vendor_completions.d/
	install -Dm644 autocomplete/complete.zsh -t "$pkgdir"/usr/share/zsh/site-functions/
}

sha512sums="
d4bb1a670487b82b1540f6ac97adbaf0c081865b5cced4775da37db3a2194dfeac629c281f3f2e3962cf5c9927dead010543769efe7b68bf9359c3cd2c5fc930  tldr-c-client-1.6.1.tar.gz
"
