#!/bin/sh

[ -d "$1" ] || exit

podman image exists aports || podman build -f Containerfile -t aports

container="$(podman run -d -it --rm -v $(dirname $(realpath $0)):/mnt aports)"
[ "$container" ] || exit

while [ $# -gt 0 ]
do
	podman exec --user test -w "/home/test" "$container" mkdir -p "aports/${1}" || break
	podman exec --user test -w "/home/test/aports/${1%%/*}" "$container" cp -r "/mnt/${1}" . || break
	podman exec --user test -w "/home/test/aports/${1}" "$container" abuild -r || break
	shift
done

podman attach "$container"

