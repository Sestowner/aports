# Contributor: omni <omni+alpine@hack.org>
# Maintainer: messense <messense@icloud.com>
pkgname=maturin
pkgver=0.15.1
pkgrel=1
pkgdesc="Build and publish crates with pyo3, rust-cpython and cffi bindings"
url="https://github.com/PyO3/maturin"
arch="all"
license="Apache-2.0 OR MIT"
makedepends="
	cargo
	dbus-dev
	openssl-dev>3
	py3-gpep517
	py3-setuptools
	py3-setuptools-rust
	py3-wheel
	"
checkdepends="py3-virtualenv py3-pyo py3-cffi libffi-dev python3-dev"
subpackages="
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	py3-$pkgname:py3:noarch
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/PyO3/maturin/archive/v$pkgver.tar.gz"

_features="--features=full,native-tls"

case "$CARCH" in
s390x)
	# s390x fails to build password-storage, so don't append it
	;;
*)
	_features="$_features,password-storage"
	;;
esac

case "$CARCH" in
riscv64)
	options="$options textrels"
	;;
esac


prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	export OPENSSL_NO_VENDOR=1

	export MATURIN_SETUP_ARGS="--no-default-features $_features"

	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2

	./target/release/maturin completions bash > $pkgname.bash
	./target/release/maturin completions fish > $pkgname.fish
	./target/release/maturin completions zsh > $pkgname.zsh
}

check() {
	export OPENSSL_NO_VENDOR=1

	cargo test \
	  --frozen \
	  --no-default-features $_features \
	  -- --skip git_sdist_generator
}

package() {
	install -Dm644 $pkgname.bash "$pkgdir"/usr/share/bash-completion/completions/$pkgname
	install -Dm644 $pkgname.fish "$pkgdir"/usr/share/fish/completions/$pkgname.fish
	install -Dm644 $pkgname.zsh "$pkgdir"/usr/share/zsh/site-functions/_$pkgname

	python3 -m installer -d "$pkgdir" \
		dist/maturin*.whl
}

py3() {
	pkgdesc="$pkgdesc (python module)"
	depends="$pkgname=$pkgver-r$pkgrel"

	amove usr/lib/python3*
}

sha512sums="
e298abb3cbfeae266c5f7c4bc8e57154527a5d040bf0276afe6e63e6d56978101ef399ab4883e7a5826d5eaa31f03019130200ca519b08d4e3bb0a12bee8eb77  maturin-0.15.1.tar.gz
"
