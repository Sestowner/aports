# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=libkcddb
pkgver=23.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://www.kde.org/applications/multimedia/"
pkgdesc="KDE CDDB library"
license="LGPL-2.0-or-later AND GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	kcmutils-dev
	kcodecs-dev
	kconfig-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	kwidgetsaddons-dev
	libmusicbrainz-dev
	qt5-qtbase-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/libkcddb-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
options="net" # Required for tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_TESTING=ON
	cmake --build build
}

check() {
	cd build
	# musicbrainztest-severaldiscs, asynccddblookuptest and synccddblookuptest are broken
	# asynchttplookuptest fails to setup dbus
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "(musicbrainztest-severaldiscs|asynchttplookuptest|asynccddblookuptest|synccddblookuptest)"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
7908710c020cee562d50472ee7d9869bc30c4669b54cfc4ce3c5d69c557050962f5155566db731ad9a34a65c9535260c3ac4777f9bcb93e590b5650a43d0fdd1  libkcddb-23.04.1.tar.xz
"
