# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=knights
pkgver=23.04.1
pkgrel=0
pkgdesc="Chess board by KDE with XBoard protocol support"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://www.kde.org/applications/games/knights/"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	kconfigwidgets-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	kio-dev
	kplotting-dev
	ktextwidgets-dev
	kwallet-dev
	kxmlgui-dev
	libkdegames-dev
	plasma-framework-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/knights-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
1ccbcae4134f86b3ff65646edc82f0819df7d0a1bc4cf9ed1a8c6d40b6953cdb24767a1ce4dcb8531b9676264e99dc033f91f1590217101b0b80246d434cac42  knights-23.04.1.tar.xz
"
