# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=knetwalk
pkgver=23.04.1
pkgrel=0
pkgdesc="Connect all the terminals to the server, in as few turns as possible"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/games/knetwalk/"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	ki18n-dev
	ktextwidgets-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	libkdegames-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/knetwalk-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
c19d6e165667b9e9d1b35e3f16f3ce02fcf5ac339a6e179adf848b7c2bdf5df9ab1743140f923c4be1cd22a24ea25d1ef1918c7e2247140927c8ba186f66df4e  knetwalk-23.04.1.tar.xz
"
