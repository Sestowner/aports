# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=libkipi
pkgver=23.04.1
pkgrel=0
pkgdesc="KDE Image Plugin Interface library"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://www.digikam.org/"
license="GPL-2.0-or-later AND LGPL-2.0-or-later"
depends_dev="
	kconfig-dev
	kservice-dev
	kxmlgui-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	libjpeg-turbo-dev
	libkexiv2-dev
	samurai
	tiff-dev
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/libkipi-$pkgver.tar.xz"
subpackages="$pkgname-dev"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
172f0a7a1943512b781cf10b6fc49e1b5ab6ab8c7fc5d7b047b21d20511aedbacde6743a638f9e00b69495f8f16a9b9773b79296f8308e7cfac96471faab3946  libkipi-23.04.1.tar.xz
"
