# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=analitza
pkgver=23.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://edu.kde.org/"
pkgdesc="A library to add mathematical features to your program"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	eigen-dev
	extra-cmake-modules
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtsvg-dev
	qt5-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/analitza-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
d58c93a3375d92fee3c66136ebaaa6d3053af4c9cc249a04e8a95af87f6022ac918c90c939c75d6a65ba9251b6deb081fb45712cc881b057ea98ac9b2fca7a63  analitza-23.04.1.tar.xz
"
