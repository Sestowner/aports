# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: Rasmus Thomsen <oss@cogitri.dev>
pkgname=tracker
pkgver=3.5.2
pkgrel=0
pkgdesc="Personal search tool and storage system"
url="https://wiki.gnome.org/Projects/Tracker"
arch="all"
license="GPL-2.0-or-later"
makedepends="
	asciidoc
	bash-completion
	dbus-dev
	glib-dev
	gobject-introspection-dev
	icu-dev
	json-glib-dev
	libsoup3-dev
	libstemmer-dev
	libxml2-dev
	meson
	py3-gobject3
	py3-setuptools
	sqlite-dev
	networkmanager-dev
	vala
	"
checkdepends="dbus"
subpackages="
	$pkgname-dbg
	$pkgname-dev
	$pkgname-doc
	$pkgname-lang
	lib$pkgname:libs
	$pkgname-bash-completion
	$pkgname-testutils:_testutils"
source="https://download.gnome.org/sources/tracker/${pkgver%.*}/tracker-$pkgver.tar.xz
	bsd-source.patch
	"

# x86: still sigabrts
# armhf: hang for a really long time
case "$CARCH" in
armhf|x86) options="$options !check" ;;
esac

build() {
	abuild-meson \
		-Db_lto=true \
		-Ddocs=false \
		-Dsystemd_user_services=false \
		-Dtests="$(want_check && echo true || echo false)" \
		. output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

check() {
	XDG_RUNTIME_DIR="$(mktemp -p "$builddir" -d)" \
	dbus-run-session -- \
	meson test -t 10 --no-rebuild --print-errorlogs -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

_testutils() {
	pkgdesc="$pkgname (test utilities)"
	depends="tracker=$pkgver-r$pkgrel py3-gobject3 bash"

	amove usr/lib/tracker-3.0/trackertestutils
}

sha512sums="
e7162116c1089f4e1a06c6bf60e515b9dd9ce174a3ab8862d52c638004a1df7feb1d69c7b958051bdcff60202eaa58c29023e9a0012c0511d75d26e76a27cd10  tracker-3.5.2.tar.xz
dfaac16da00496f580f864547dce6740a3294cca2f56853ec965e7682d1b9f1fce56f86c0d729e9613d601b80f71d2a562c5921d311681bdc9a0c442142773ee  bsd-source.patch
"
