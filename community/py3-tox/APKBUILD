# Contributor: Stuart Cardall <developer@it-offshore.co.uk>
# Maintainer: Stuart Cardall <developer@it-offshore.co.uk>
pkgname=py3-tox
_pkgname=${pkgname#py3-*}
pkgver=4.5.1
pkgrel=0
pkgdesc="virtualenv management and test command line tool"
options="!check" #  Requires community/py3-pathlib2, and unpackaged flaky
url="https://tox.readthedocs.org/"
arch="noarch"
license="MIT"
depends="
	py3-cachetools
	py3-chardet
	py3-colorama
	py3-filelock
	py3-packaging
	py3-platformdirs
	py3-pluggy
	py3-pyproject-api
	py3-virtualenv
	python3
	"
makedepends="
	py3-gpep517
	py3-hatch-vcs
	py3-hatchling
	"
checkdepends="py3-pytest"
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/${_pkgname:0:1}/$_pkgname/$_pkgname-$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

replaces="py-tox" # Backwards compatibility
provides="py-tox=$pkgver-r$pkgrel" # Backwards compatibility

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 setup.py test
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/*.whl
}

sha512sums="
75d495d6dba134c3fe537fc190c1f428ff188ee67c346911c02ee151a8369425b22956eea52be5d434735e1590edce57b3487d5f4b277022bf8cfe8bf4660cf8  tox-4.5.1.tar.gz
"
