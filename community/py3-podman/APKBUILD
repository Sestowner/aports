# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=py3-podman
pkgver=4.5.0
pkgrel=0
pkgdesc="Python bindings for Podman's RESTful API"
url="https://github.com/containers/podman-py"
license="Apache-2.0"
arch="noarch"
depends="python3 py3-requests py3-urllib3 py3-xdg"
makedepends="py3-gpep517 py3-setuptools py3-wheel"
checkdepends="py3-pytest py3-requests-mock py3-fixtures"
subpackages="$pkgname-pyc"
source="https://github.com/containers/podman-py/archive/v$pkgver/py3-podman-$pkgver.tar.gz"
builddir="$srcdir/podman-py-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	pytest podman/tests/unit
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/podman-$pkgver-py3-none-any.whl
}

sha512sums="
903466e11afebd26da65cc363ab49a56255731ea8bc07d202dd80907e1dd6704b5a8542c5a417f3c46ec93882bb21ef6990ae80fe558dfd3310b9272590cbd39  py3-podman-4.5.0.tar.gz
"
