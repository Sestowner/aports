# Contributor: Jonah Brüchert <jbb@kaidan.im>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=angelfish
pkgver=23.04.1
pkgrel=0
pkgdesc="Small Webbrowser for Plasma Mobile"
# armhf blocked by extra-cmake-modules
# ppc64le and s390x blocked by qt5-qtwebengine
# riscv64 disabled due to missing rust in recursive dependency
arch="all !ppc64le !s390x !armhf !riscv64"
url="https://phabricator.kde.org/source/plasma-angelfish/"
license="GPL-3.0-or-later"
depends="
	kirigami2
	plasma-framework
	purpose
	qt5-qtbase-sqlite
	qt5-qtfeedback
	qt5-qtquickcontrols2
	"
makedepends="
	corrosion
	extra-cmake-modules
	kdeclarative-dev
	ki18n-dev
	kio-dev
	kirigami-addons-dev
	kirigami2-dev
	plasma-framework-dev
	purpose-dev
	qqc2-desktop-style-dev
	qt5-qtfeedback-dev
	qt5-qtwebengine-dev
	samurai
	"
subpackages="$pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/angelfish-$pkgver.tar.xz"
options="net" # net required to download Rust dependencies

provides="plasma-angelfish=$pkgver-r$pkgrel" # Backwards compatibility
replaces="plasma-angelfish" # Backwards compatibility

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -j1
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
3b23364dfb6feed6deb5691dc7a69e35b9cb4b1ba6588b43e3c4bf9316007c1719a52aab18bd04f43d069cc01e3cad5b5603a65eb22d7fc5e26e739f6097937c  angelfish-23.04.1.tar.xz
"
