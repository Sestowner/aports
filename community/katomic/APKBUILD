# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=katomic
pkgver=23.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/games/org.kde.katomic"
pkgdesc="A fun and educational game built around molecular geometry"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	qt5-qtbase-dev
	kcoreaddons-dev
	kconfig-dev
	kcrash-dev
	kwidgetsaddons-dev
	ki18n-dev
	kxmlgui-dev
	knewstuff-dev
	kdoctools-dev
	kdbusaddons-dev
	libkdegames-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/katomic-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
c65a2f7aa92c5a65daacb0eddd0ca759f91e8129ac6ffbb8149ec474e3070e02c72681157c678ab008e513f2bc24772fc342f1647328a8bba627122917b13cc0  katomic-23.04.1.tar.xz
"
