# Contributor: Sergiy Stupar <owner@sestolab.pp.ua>
# Maintainer: Sergiy Stupar <owner@sestolab.pp.ua>
pkgname=teeworlds
pkgver=0.7.5
pkgrel=0
pkgdesc="Retro multiplayer shooter"
url="https://www.teeworlds.com"
arch="all"
license="custom AND CC-BY-SA-3.0"
depends="$pkgname-data"
makedepends="cmake freetype-dev sdl2-dev wavpack-dev"
checkdepends="gtest-dev"
subpackages="$pkgname-server $pkgname-data::noarch $pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/teeworlds/teeworlds/releases/download/$pkgver/teeworlds-$pkgver-src.tar.gz"
builddir="$srcdir/$pkgname-$pkgver-src"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build
}

check() {
	cmake --build build -t run_tests
}

package() {
	DESTDIR="$pkgdir" cmake --install build
	install -Dm644 license.txt "$pkgdir"/usr/share/licenses/$pkgname/LICENSE
}

server() {
	pkgdesc="Teeworlds server"
	depends="$pkgname-data"
	amove usr/bin/teeworlds_srv
}

data() {
	pkgdesc="Data files for Teeworlds"
	amove usr/share/teeworlds
}

sha512sums="
f0c55489b99b06e27298ac42adcf31e1130e7eace6f8672836e50c66c27ab0ccce5b8680c7181a2adc59e1f5808564001b5f0bc8ac7ab355ad8db71328ea7a76  teeworlds-0.7.5.tar.gz
"
